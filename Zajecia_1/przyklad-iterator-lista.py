
# class List:
#    def __init__
#    def __iter__
#    def index


class IteratorListy:
	def __init__(self, lista_na_ktorej_operuje):
		self.lista = lista_na_ktorej_operuje
		self.idx = 0
	
	def __next__(self):
		if len(self.lista) > self.idx:
			t = self.lista[self.idx]
			self.idx += 1
			return t
		else:
			raise StopIteration
	
	def __iter__(self):
		return self


# idx 0   1    2
x = ['a', 'b', 'c']

it = IteratorListy(x)

for element in it:
	print(element)

# __next__ -> a
# __next__ -> b
# __next__ -> c
# __next__ -> None
	