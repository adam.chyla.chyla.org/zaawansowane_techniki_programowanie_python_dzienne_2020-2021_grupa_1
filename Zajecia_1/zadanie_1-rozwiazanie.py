from itertools import islice

class count:
	def __init__(self, start): 
		self.start = start

	def __next__(self):
		t = self.start
		self.start += 1
		return t
	
	def __iter__(self):
		return self


poczatek = int(input("Podaj wartosc poczatkowa: "))

licz = count(poczatek)

for element in islice(licz, 0, 10):
	print(element)
