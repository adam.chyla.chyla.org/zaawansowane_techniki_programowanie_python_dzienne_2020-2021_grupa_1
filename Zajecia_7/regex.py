import re

# search
# match
# findall

text = "Alicja ma kota i psa"

# przyklad search
# wyszukaj pierwsze slowo zaczynajace
# sie na litere 'k', a konczace na 'a'
match = re.search(pattern="k.+?a",
                  string=text)
if match:
    print("search: znaleziono slowo:", match.group(0))
else:
    print("search: nie znaleziono")


# przyklad match
# czy tekst jest poprawnym kodem pocztowy, XX-XXX

text = "13-345sss"

match = re.match(pattern="[0-9]{2}-\d{3}",
                 string=text)

if match:
    print("match: tak:", match.group(0))
else:
    print("match: nie")


# przyklad match
# wybierz liczby z kodu pocztowego, XX-XXX

text = "13-345sss"

match = re.match(pattern="(\d{2})-(\d{3})",
                 string=text)

if match:
    print("match2: tak:", match.group(0))
    print("match2: match.group(1):", match.group(1))
    print("match2: match.group(2):", match.group(2))
else:
    print("match2: nie")


# przyklad findall
# wyszukaj wszystkie liczby w tekscie

text = "Alicja ma 5 kotow i 10 psow"

cos = re.findall("\d+", text)
print("findall:", cos)

