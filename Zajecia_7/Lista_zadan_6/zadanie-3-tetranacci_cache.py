def tetranacci_cache(f):
    cache = {}

    def wrapper(n):
        if n in cache:
            #print(f"Dla n={n}, wynik mam w cache")
            return cache[n]
        else:
            #print(f"Dla n={n} musze policzyc wynik")
            val = f(n)
            cache[n] = val
            return val

    return wrapper


@tetranacci_cache
def tetranacci(n):
    poczatkowe = [0, 0, 0, 1]
    if n < 0:
        return 0
    elif n < len(poczatkowe):
        return poczatkowe[n]
    else:
        return (tetranacci(n-1) + tetranacci(n-2)
                   + tetranacci(n-3) + tetranacci(n-4))


def test_tetranacci(n, oczekiwany_wynik):
    element_ciagu = tetranacci(n)
    assert element_ciagu == oczekiwany_wynik, f"tetranacci({n}) powinno wynosic: {oczekiwany_wynik}; otrzymano: {element_ciagu}"

test_tetranacci(n=-1, oczekiwany_wynik=0)
test_tetranacci(n=0, oczekiwany_wynik=0)
test_tetranacci(n=1, oczekiwany_wynik=0)
test_tetranacci(n=2, oczekiwany_wynik=0)
test_tetranacci(n=3, oczekiwany_wynik=1)
test_tetranacci(n=4, oczekiwany_wynik=1)
test_tetranacci(n=5, oczekiwany_wynik=2)
test_tetranacci(n=7, oczekiwany_wynik=8)
