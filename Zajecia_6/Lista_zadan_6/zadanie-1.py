def hello(f):
    def nowa_funkcja():
        print("hello")
#        print("przed wykonaniem")
        f()
#        print("po wykonaniu")
    
    return nowa_funkcja


# moja_funkcja = hello(moja_funkcja)
@hello
def moja_funkcja():
    print("moja funkcja")
    

moja_funkcja()
moja_funkcja()
moja_funkcja()
