liczba = 6 # --> int

#                           /----------------------------------------------
#                          \/                                             |
#                  obiekt funkcji hello    obiekt funkcji druga    obiekt funkcji wewnetrzna
#                   /\                      /\          \------------/
# n     |  w        /                       /
# hello |   *-------                       / 
# druga |   *-----------------------------/
#

def druga(zmienna):
    def funkcja_wewnetrzna():
        print("funkcja wewnetrzna:", zmienna)
        zmienna()

    print("jestem druga")
    return funkcja_wewnetrzna

@druga
def hello():
    print("hello")
    
#hello = druga(hello)

hello()
