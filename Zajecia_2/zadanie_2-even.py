from itertools import islice

def even(x = 0):
	if x % 2 == 1:
		raise ValueError("podano liczbe nieparzysta")
		
	while True:
		yield x
		x += 2
		
for i in islice(even(), 5):
	print(i)
	
for i in islice(even(1), 5):
	print(i)
