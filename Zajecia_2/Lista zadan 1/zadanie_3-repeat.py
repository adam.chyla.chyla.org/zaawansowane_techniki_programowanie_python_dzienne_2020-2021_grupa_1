
class repeat:
	def __init__(self, obj, times=None):
		self.obj = obj
		self.times = times
		self.count = 0
	
	def __next__(self):
		if self.times is not None and self.count == self.times:
			raise StopIteration
		else:
			self.count += 1
			return self.obj
	
	def __iter__(self):
		return self

	
for x in repeat(10, 8):
	print("repeat(10):", x)

for x in repeat(15):
	print("repeat(15):", x)