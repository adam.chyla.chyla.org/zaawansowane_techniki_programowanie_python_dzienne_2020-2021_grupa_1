class tetranacci:
	def __init__(self, limit):
		self.wyrazy = [0, 0, 0, 1]
		self.limit = limit
		self.liczba_wyrazow_zwroconych = 0

	def __next__(self):
		if self.limit == self.liczba_wyrazow_zwroconych:
			raise StopIteration
		else:
			nowa_suma = sum(self.wyrazy)
			self.wyrazy.append(nowa_suma)

			element = self.wyrazy[0]
			del(self.wyrazy[0])

			self.liczba_wyrazow_zwroconych += 1
			return element

	def __iter__(self):
		return self


t = tetranacci(5)
t2 = tetranacci(9)

for x in t:
	print("t1:", x)
	
for x in t2:
	print("t2:", x)
	