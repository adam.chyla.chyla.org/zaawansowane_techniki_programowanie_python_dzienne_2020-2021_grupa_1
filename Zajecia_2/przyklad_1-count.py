# Generator
def count():
	x = 0
	while True:
		yield x
		x += 1

ctx = count()
# print(dir(ctx))

for x in ctx: # 0, 1, 2, 3, 4
	print("for:", x)
