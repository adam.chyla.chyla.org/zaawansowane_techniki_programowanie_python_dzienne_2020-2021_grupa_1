from threading import Thread, Lock
from time import sleep

def count(thread_id, results, results_lock):
	results_lock.acquire()
	
	for i in range(10*thread_id, 10*thread_id+9):
		print(f"Watek {thread_id}: {i}")
		results.append(i)
		sleep(thread_id)
		
	results_lock.release()

results = []
results_lock = Lock()

threads = [
	Thread(target=count, args=(1, results, results_lock)),
	Thread(target=count, args=(2, results, results_lock)),
	Thread(target=count, args=(3, results, results_lock)),
	Thread(target=count, args=(4, results, results_lock))
]

for thread in threads:
	thread.start()
	
for thread in threads:
	thread.join()

print(results)
