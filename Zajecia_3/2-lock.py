import threading
import time

suma = 0
lock = threading.Lock()

def count(nr_watku):
	for i in range(10):
		lock.acquire()
		global suma
		suma = suma + i
		print(f"Watek {nr_watku}, i={i}: {suma}")
		lock.release()
		
		time.sleep(nr_watku)

t1 = threading.Thread(target=count, args=(1,))
t2 = threading.Thread(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()

