# Zadanie 3 z listy 2

def accumulate(it):
	suma = None
	for x in it:
		if suma is None:
			suma = x
		else:
			suma = suma + x
		yield suma

for x in accumulate([1, 2, 3, 4, 5]): # --> 1 3 6 10 15
	print(x)

for x in accumulate(["ala ", "ma", " kota"]):
	print(x)

	