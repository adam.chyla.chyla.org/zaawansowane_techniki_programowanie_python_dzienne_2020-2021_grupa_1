import threading
import time

def count(nr_watku):
	for i in range(10):
		print(f"Pracuje {nr_watku}, liczba {i}...")
		time.sleep(nr_watku)
		
t1 = threading.Thread(target=count, args=(1,))
t2 = threading.Thread(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()

