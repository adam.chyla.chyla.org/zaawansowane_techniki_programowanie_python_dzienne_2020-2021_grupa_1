from concurrent.futures import ThreadPoolExecutor
import time


liczby = [1, 2, 3, 4, 5, 6, 7]

def dodawanie(jedna_liczba):
	return jedna_liczba + 10

with ThreadPoolExecutor(max_workers=1) as executor:
	gen = executor.map(dodawanie, liczby)
	print(list(gen))

