from concurrent.futures import ThreadPoolExecutor
import time
import threading

suma = 0
lock = threading.Lock()

def count(nr_watku):
	for i in range(10):
		lock.acquire()
		global suma
		suma = suma + i
		print(f"Watek {nr_watku}, i={i}: {suma}")
		lock.release()
		
		time.sleep(nr_watku)

with ThreadPoolExecutor(max_workers=2) as executor:
	executor.submit(count, 1)
	executor.submit(count, 2)
	executor.submit(count, 3)
	executor.submit(count, 4)

