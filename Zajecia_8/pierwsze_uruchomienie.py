import sqlite3

conn = sqlite3.connect("dane_programu.db")

conn.execute("""
CREATE TABLE IF NOT EXISTS ustawienia (
     id INTEGER PRIMARY KEY,
     nazwa TEXT UNIQUE,
     wartosc INTEGER);
""")

# ustawienia
# ... | czy_pierwsze_uruchomienie | 0

c = conn.cursor()
c.execute("SELECT wartosc FROM ustawienia WHERE nazwa=?;",
              ('czy_pierwsze_uruchomienie',))
results = c.fetchall() # lista z wynikami

print("wyniki:", results)

if len(results) == 0 or results[0][0] == 1:
    c = conn.cursor()
    c.execute('INSERT OR REPLACE INTO ustawienia VALUES (null, ?, ?);',
              ('czy_pierwsze_uruchomienie', 0))
    conn.commit()
    print("uruchomiono pierwszy raz!")
else:
    print("uruchomiono kolejny raz")

conn.close()
