# przyklad na sume trzech silni
# sum(12!, 19!, 20!)

from concurrent.futures import ProcessPoolExecutor
import time
import math

def main():
	input_data = [12, 19, 20]
	
	with ProcessPoolExecutor(max_workers=5) as pool:
		output_data = pool.map(math.factorial, input_data)
		
	print("Suma:", sum(output_data))
	
if __name__ == "__main__":
	main()
