# przyklad na sume trzech silni
# sum(12!, 19!, 20!)

import multiprocessing
import time
import math

def subprocess(queue, n):
	result = math.factorial(n)
	queue.put(result)


def queue_sum(queue):
	sum = 0
	while not queue.empty():
		item = queue.get()
		sum += item
	return sum
	

def main():
	queue = multiprocessing.Queue()
	processes = [
		multiprocessing.Process(target=subprocess, args=(queue, 12,)),
		multiprocessing.Process(target=subprocess, args=(queue, 19,)),
		multiprocessing.Process(target=subprocess, args=(queue, 20,)),
	]

	for p in processes:
		p.start()
		
	for p in processes:
		p.join()

	print("Suma:", queue_sum(queue))

if __name__ == "__main__":
	main()

