import urllib.request
import threading
import time


def download_one_webpage(url):
    with urllib.request.urlopen(url) as f:
        source = f.read().decode('utf-8')
        return source
	

def download_multiple_webpages(urls_to_download):
	all_sources = []
	for url in urls_to_download:
		source = download_one_webpage(url)
		all_sources.append(source)
	return all_sources


def download_pages(urls_to_download, all_sources):
	sources = download_multiple_webpages(urls_to_download)
	all_sources.extend(sources)


def read_urls_to_download_from_user():
	urls_to_download = []
	for i in range(1, 6):
		url = input("Podaj adres URL (np. https://python.org/): ")
		urls_to_download.append(url)
	return urls_to_download

def spawn_new_thread_and_wait_for_download(urls_to_download, all_sources):
	print("Rozpoczynanie pobierania...")
	thread = threading.Thread(target=download_pages,
							  args=(urls_to_download,
									all_sources,))
	thread.start()

	while thread.is_alive():
		print("Oczekiwanie na zakonczenie pobierania...")
		time.sleep(1)

	thread.join()
	print("Wszystko pobrano.")


def print_number_of_characters_for_each_page(urls_to_download, all_sources):
	for url, source in zip(urls_to_download, all_sources):
		print("liczba znakow dla {}: {}".format(url, len(source)))


def main():
	urls_to_download = read_urls_to_download_from_user()

	all_sources = []
	spawn_new_thread_and_wait_for_download(urls_to_download, all_sources)

	print_number_of_characters_for_each_page(urls_to_download, all_sources)
	

if __name__ == "__main__": # czy skrypt uruchomil czlowiek
	main()
