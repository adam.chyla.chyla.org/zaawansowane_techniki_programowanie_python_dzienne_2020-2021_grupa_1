import multiprocessing
import time

def count(nr_procesu):
	for i in range(10):
		print(f"Pracuje {nr_procesu}, liczba {i}...")
		time.sleep(nr_procesu)

t1 = multiprocessing.Process(target=count, args=(1,))
t2 = multiprocessing.Process(target=count, args=(2,))

t1.start()
t2.start()

t1.join()
t2.join()

