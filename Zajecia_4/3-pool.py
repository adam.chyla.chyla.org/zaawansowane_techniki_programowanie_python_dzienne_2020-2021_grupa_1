# przyklad na sume trzech silni
# sum(12!, 19!, 20!)

import multiprocessing
import time
import math

def main():
	input_data = [12, 19, 20]
	
	with multiprocessing.Pool(processes=5) as pool:
		output_data = pool.map(math.factorial, input_data)
		
	print("Suma:", sum(output_data))
	
if __name__ == "__main__":
	main()
