import multiprocessing
import os


def subprocess():
	print("PID potomka:", os.getpid())


def main():
	print("PID rodzica:", os.getpid())
	
	p1 = multiprocessing.Process(target=subprocess)
	p2 = multiprocessing.Process(target=subprocess)
	
	p1.start()
	p2.start()
	
	p1.join()
	p2.join()


if __name__ == "__main__":
	main()
